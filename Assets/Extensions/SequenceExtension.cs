﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
// ReSharper disable once PartialTypeWithSinglePart
public static partial class SequenceExtension
{
    #region -- sequence --------------------------------------

    #region do scale

    public static Sequence DoScaleSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScale(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoScaleSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScale(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do scale x

    public static Sequence DoScaleXSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScaleX(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoScaleXSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScaleX(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do scale y

    public static Sequence DoScaleYSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScaleY(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoScaleYSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScaleY(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do scale z

    public static Sequence DoScaleZSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScaleZ(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoScaleZSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoScaleZ(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do punch scale

    public static Sequence DoPunchScaleSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        Ease ease = Ease.OutQuad,
        int vibrato = 2,
        float elasticity = 0.5f,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoPunchScale(value, duration, vibrato, elasticity)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoPunchScaleSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        AnimationCurve ease,
        int vibrato = 2,
        float elasticity = 0.5f,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoPunchScale(value, duration, vibrato, elasticity)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do puch position

    public static Sequence DoPunchPositionSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        Ease ease = Ease.OutQuad,
        int vibrato = 2,
        float elasticity = 0.5f,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoPunchPosition(value, duration, vibrato, elasticity)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoPunchPositionSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        AnimationCurve ease,
        int vibrato = 2,
        float elasticity = 0.5f,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoPunchPosition(value, duration, vibrato, elasticity)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do move

    public static Sequence DoMoveSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMove(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoMoveSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMove(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do move x

    public static Sequence DoMoveXSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMoveX(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoMoveXSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMoveX(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do move y

    public static Sequence DoMoveYSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMoveY(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoMoveYSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMoveY(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do move y

    public static Sequence DoMoveZSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMoveZ(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoMoveZSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoMoveZ(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do local move

    public static Sequence DoLocalMoveSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMove(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoLocalMoveSequence(
        this Transform transform,
        Vector2 value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMove(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do local move x

    public static Sequence DoLocalMoveXSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMoveX(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoLocalMoveXSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMoveX(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do local move y

    public static Sequence DoLocalMoveYSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMoveY(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoLocalMoveYSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMoveY(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do local move y

    public static Sequence DoLocalMoveZSequence(
        this Transform transform,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMoveZ(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoLocalMoveZSequence(
        this Transform transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoLocalMoveZ(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do fade

    public static Sequence DoFadeSequence(
        this Image image,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(image
                    .DoFade(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoFadeSequence(
        this Image transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoFade(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #region do fillamount

    public static Sequence DoFillAmountSequence(
        this Image image,
        float value,
        float duration,
        Ease ease = Ease.OutQuad,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(image
                    .DoFillAmount(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    public static Sequence DoFillAmountSequence(
        this Image transform,
        float value,
        float duration,
        AnimationCurve ease,
        int loop = 1,
        Action onStart = null,
        Action onError = null)
    {
        var sequence = DoTween.Sequence();
        try
        {
            sequence.Append(transform
                    .DoFillAmount(value, duration)
                    .SetEase(ease))
                .SetLoops(loop)
                .SetRelative(false)
                .OnPlay(() => onStart?.Invoke());
        }
        catch (Exception)
        {
            sequence?.Kill();
            onError?.Invoke();
        }

        return sequence;
    }

    #endregion

    #endregion
}