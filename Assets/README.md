# Dotween Extension

## What
	- Collection scripts extension of DOTween for Unity

## Installation

```bash
"com.yenmoc.dotween-extension":"https://gitlab.com/yenmoc/dotween-extension"
or
npm publish --registry=http://localhost:4873
```

## Usages

* ```csharp
 	.PlayAsObservable()
 	.Subscribe(_ => {
 		//onComplete
 	}).AddTo(this);
 ```
