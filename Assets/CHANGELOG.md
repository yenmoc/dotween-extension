# Changelog
All notable changes to this project will be documented in this file.

## [0.0.9] - 01-09-2019
### Added
	-Add DoLocalMoveXSequence, DoLocalMoveYSequence, DoLocalMoveZSequence, DoMoveXSequence, DoMoveYSequence, DoMoveZSequence method

## [0.0.8] - 01-09-2019
### Changed
	-Update dependencies

## [0.0.7] - 31-08-2019
### Added
	-add DoScaleXSequence, DoScaleYSequence, DoScaleZSequence. Update assembly definition

## [0.0.3] - 31-08-2019
### Removed
	-remove assembly definition

## [0.0.2] - 31-08-2019
### Added
	-add SequenceExtension

### Changed
	-rename DOTweenExtension to DoTweenExtension


## [0.0.1] - 31-08-2019
### Added
	-add DOTweenExtension

